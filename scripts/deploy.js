const hre = require("hardhat");

async function main() {
  const TRKBEP20 = await hre.ethers.getContractFactory("TRKBEP20")
  const tRKBEP20 = await TRKBEP20.deploy("TOREKKO", "TRK")
  const TorekkoVesting = await hre.ethers.getContractFactory("TorekkoVesting");
  const torekkoVesting = await TorekkoVesting.deploy(tRKBEP20.address,
  "0x656835e34A5332BB07Be9c0eF8f3d2d31f821d59");

  console.log("TRKBEP20 deployed to:", tRKBEP20.address);
  console.log("torekkoVesting deployed to:", torekkoVesting.address);

}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
