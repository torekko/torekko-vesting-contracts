// SPDX-License-Identifier: MIT
pragma solidity 0.8.7;

import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "@openzeppelin/contracts/utils/math/Math.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "./TRKBEP20.sol";

contract TorekkoVesting is ReentrancyGuard {
    using Math for uint256;
    using SafeMath for uint256;
    using SafeERC20 for TRKBEP20;

    struct VestingType {
        bool created;
        uint256 initialRelease;
        uint256 releaseRate;
        uint256 claimPeriod;
        uint256 numberOfClaims;
        uint256 initialCooldown;
    }

    struct Vesting {
        string type_;
        uint256 amount;
        uint256 creationTime;
        uint256 rate;
        uint256 initialRelease;
        uint256 claimPeriod;
        uint256 firstClaim;
        uint256 totalClaims;
        uint256 numberOfClaims;
    }

    mapping(string => VestingType) public vestingTypes;
    mapping(address => Vesting) public vestings;
    mapping(address => string) public vesters;
    mapping(address => address[]) public acceptRemoveVester;
    mapping(string => address[]) public acceptRemoveVesting;
    mapping(address => address[]) public acceptAddAdmin;
    mapping(uint256 => address[]) public acceptNewNum;
    mapping(address => bool) public admins;

    uint256 public totalAdmins;
    uint256 public minAdmin;
    address public reserve;

    TRKBEP20 public trkBEP20;

    address private _owner;

    event OwnershipTransferred(
        address indexed previousOwner,
        address indexed newOwner
    );
    event VesterAdded(address vester, string vestingType, uint256 amount);
    event VestingTypeRemoved(string type_);
    event Claimed(uint256 amount);
    event Removed(address vester);
    event AdminRevoked(address admin);
    event Voted(address voted, bool worked);
    event VotedVestingType(string vestingType, bool worked);
    event VotedNum(uint256 voted, bool worked);
    event Added(address newAdmin);
    event Changed(uint256 newNum);
    event ChangedReserve(address reserve_);
    event VestingTypeAdded(
        string type_,
        uint256 initialRelease,
        uint256 releaseRate,
        uint256 claimPeriod,
        uint256 numberOfClaims,
        uint256 initialCooldown
    );

    ///@dev Verifies that the user can claim the initial release
    modifier canClaimFirst() {
        require(vestings[msg.sender].firstClaim > 0, "Already claimed");
        require(
            vestings[msg.sender].creationTime.add(
                vestings[msg.sender].claimPeriod
            ) <= block.timestamp,
            "Too early"
        );
        _;
    }

    ///@dev Verifies that the user can claim his regular vesting
    modifier canClaim() {
        require(vestings[msg.sender].numberOfClaims > 0, "Vesting is finished");
        uint256 alreadyClaimed = vestings[msg.sender].totalClaims.sub(
            vestings[msg.sender].numberOfClaims
        );
        uint256 lastClaim = vestings[msg.sender].creationTime.add(
            vestings[msg.sender].claimPeriod.mul(alreadyClaimed)
        );
        require(
            block.timestamp.sub(lastClaim, "Too early") >
                vestings[msg.sender].claimPeriod,
            "Too early"
        );
        _;
    }

    ///@dev Verifies that the user is admin
    modifier onlyAdmin() {
        require(admins[msg.sender], "Not admin");
        _;
    }

    ///@dev Verifies that enough admins have voted for the vester to be removed
    ///@param vester- Address of the vester that has to be removed
    modifier canBeRemoved(address vester) {
        require(
            acceptRemoveVester[vester].length >= minAdmin,
            "Not enough votes"
        );
        _;
    }

    ///@dev Verifies that the voter is admin and hasn't voted yet
    ///@param vester- Address of the vester that has to be removed
    modifier canVoteRemove(address vester) {
        for (uint256 i = 0; i < acceptRemoveVester[vester].length; i++) {
            require(
                !(msg.sender == acceptRemoveVester[vester][i]),
                "Already voted"
            );
        }
        _;
    }

    ///@dev Verifies that enough admins have voted for the type vesting to be removed
    ///@param vestingType - Type of the vesting that has to be removed
    modifier canVestingBeRemoved(string calldata vestingType) {
        require(
            acceptRemoveVesting[vestingType].length >= minAdmin,
            "Not enough votes"
        );
        _;
    }

    ///@dev Verifies that the voter is admin and hasn't voted yet
    ///@param vestingType - Type of the vesting that has to be removed
    modifier canVoteRemoveVesting(string calldata vestingType) {
        for (uint256 i = 0; i < acceptRemoveVesting[vestingType].length; i++) {
            require(
                !(msg.sender == acceptRemoveVesting[vestingType][i]),
                "Already voted"
            );
        }
        _;
    }

    ///@dev Verifies that enough admins have voted for the admin to be added
    ///@param newAdmin- Address of the admin that has to be added
    modifier canBeAdded(address newAdmin) {
        require(
            acceptAddAdmin[newAdmin].length >= minAdmin,
            "Not enough votes"
        );
        _;
    }

    ///@dev Verifies that the voter is admin and hasn't voted yet
    ///@param newAdmin- Address of the admin that has to be added
    modifier canVoteAdd(address newAdmin) {
        for (uint256 i = 0; i < acceptAddAdmin[newAdmin].length; i++) {
            require(
                !(msg.sender == acceptAddAdmin[newAdmin][i]),
                "Already voted"
            );
        }
        _;
    }

    ///@dev Verifies that enough admins have voted for the number of admins needed for a decision
    ///@param newNum- New number of admins that have to vote for a decision
    modifier canBeChanged(uint256 newNum) {
        require(acceptNewNum[newNum].length >= minAdmin, "Not enough votes");
        _;
    }

    ///@dev Verifies that the voter is admin and hasn't voted yet
    ///@param newNum- New number of admins that have to vote for a decision
    modifier canVoteChange(uint256 newNum) {
        for (uint256 i = 0; i < acceptNewNum[newNum].length; i++) {
            require(!(msg.sender == acceptNewNum[newNum][i]), "Already voted");
        }
        _;
    }

    /**
     * @dev Throws if called by any account other than the owner.
     */
    modifier onlyOwner() {
        require(_owner == msg.sender, "Ownable: caller is not the owner");
        _;
    }

    ///@dev Verify that a vestingType exists
    ///@param vestingType - Type of vesting
    modifier isVestingType(string calldata vestingType) {
        require(vestingTypes[vestingType].created, "Type not exist");
        _;
    }

    constructor(TRKBEP20 trkBEP20_, address reserve_) {
        trkBEP20 = trkBEP20_;
        reserve = reserve_;
        minAdmin = 1;
        admins[msg.sender] = true;
        totalAdmins++;
        _setOwner(msg.sender);
    }

    /**
     * @dev Leaves the contract without owner. It will not be possible to call
     * `onlyOwner` functions anymore. Can only be called by the current owner.
     *
     * NOTE: Renouncing ownership will leave the contract without an owner,
     * thereby removing any functionality that is only available to the owner.
     */
    function renounceOwnership() public onlyOwner {
        admins[msg.sender] = false;
        totalAdmins--;
        _setOwner(address(0));
    }

    /**
     * @dev Returns the address of the current owner.
     */
    function owner() public view returns (address) {
        return _owner;
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     * Can only be called by the current owner.
     */
    function transferOwnership(address newOwner) public onlyOwner {
        require(
            newOwner != address(0),
            "Ownable: new owner is the zero address"
        );
        admins[msg.sender] = false;
        admins[newOwner] = true;
        _setOwner(newOwner);
    }

    function _setOwner(address newOwner) private {
        address oldOwner = _owner;
        _owner = newOwner;
        emit OwnershipTransferred(oldOwner, newOwner);
    }

    ///@dev Vote to add an admin
    ///@param newAdmin - admin that has to be added
    ///@return bool
    function voteAddAdmin(address newAdmin)
        external
        onlyAdmin
        canVoteAdd(newAdmin)
        returns (bool)
    {
        acceptAddAdmin[newAdmin].push(msg.sender);
        emit Voted(newAdmin, true);
        return true;
    }

    ///@dev Set the new reserve address
    ///@param reserve_ - new reserve address
    ///@return bool
    function setReserve(address reserve_) external onlyAdmin returns (bool) {
        reserve = reserve_;
        emit ChangedReserve(reserve);
        return true;
    }

    ///@dev revoke admin role from an existing admin
    ///@param admin - address of admin to be removed
    ///@return bool
    function removeAdmin(address admin) external onlyOwner returns (bool) {
        require(admin != _owner, "Vesting: owner can not be removed ");
        require(admins[admin], "Vesting : not an admin");
        admins[admin] = false;
        totalAdmins--;
        emit AdminRevoked(admin);
        return true;
    }

    ///@dev Add an admin if enough admins have voteed
    ///@param newAdmin - admin that will be added
    ///@return bool
    function addAdmin(address newAdmin)
        external
        onlyAdmin
        canBeAdded(newAdmin)
        returns (bool)
    {
        delete acceptAddAdmin[newAdmin];
        admins[newAdmin] = true;
        totalAdmins++;
        emit Added(newAdmin);
        return true;
    }

    ///@dev Vote to set the number of admins that have to accept a decision
    ///@param newNum - Number of admins that have to accept a decision
    ///@return bool
    function voteSetAdmin(uint256 newNum)
        external
        onlyAdmin
        canVoteChange(newNum)
        returns (bool)
    {
        acceptNewNum[newNum].push(msg.sender);
        emit VotedNum(newNum, true);
        return true;
    }

    ///@dev Set the number of admins that have to accept a decision if enough admins have voted
    ///@param newNum - Number of admins that have to accept a decision
    ///@return bool
    function setAdminMin(uint256 newNum)
        external
        onlyAdmin
        canBeChanged(newNum)
        returns (bool)
    {
        require(
            newNum <= totalAdmins,
            "Vesting: Number of signature should be less or equal than to admins total"
        );
        delete acceptNewNum[newNum];
        minAdmin = newNum;
        emit Changed(newNum);
        return true;
    }

    ///@dev Add a new vester
    ///@param vester - address of the vester
    ///@param vesting - type of vesting
    ///@param amount - amount of the vesting
    ///@return bool
    function addVester(
        address vester,
        string calldata vesting,
        uint256 amount
    ) external onlyAdmin isVestingType(vesting) returns (bool) {
        VestingType storage vestingType = vestingTypes[vesting];
        uint256 initial = vestingType.initialRelease.mul(amount);
        initial = initial.div(uint256(10000));
        uint256 remaining = amount.sub(initial);
        vestings[vester] = Vesting(
            vesting,
            remaining,
            block.timestamp.add(vestingType.initialCooldown).sub(
                vestingType.claimPeriod
            ),
            vestingType.releaseRate,
            initial,
            vestingType.claimPeriod,
            initial,
            vestingType.numberOfClaims,
            vestingType.numberOfClaims
        );
        vesters[vester] = vesting;
        emit VesterAdded(vester, vesting, amount);
        return true;
    }

    ///@dev Add multiple new vester
    ///@param vester - address of the vesters
    ///@param vesting - type of vestings
    ///@param amounts - amounts of the vesting
    /// Please make that the vesting type already exists
    ///@return bool
    function addMultipleVesters(
        address[] memory vester,
        string[] calldata vesting,
        uint256[] memory amounts
    ) external onlyAdmin returns (bool) {
        require(
            vester.length == vesting.length && vesting.length == amounts.length,
            "Vesting: all arrays need to have the same length"
        );
        for (uint256 i = 0; i < vester.length; i++) {
            this.addVester(vester[i], vesting[i], amounts[i]);
        }
        return true;
    }

    ///@dev Add a vesting type
    ///@param type_ - type of vesting
    ///@param initialRelease - initial release percentage
    ///@param releaseRate - release percentage / period of release
    ///@param claimPeriod - period between 2 claims
    ///@param numberOfClaims_ - number of claims during the lock period
    ///@param initialCooldown - cliff time
    ///@return bool
    function addVestingType(
        string calldata type_,
        uint256 initialRelease,
        uint256 releaseRate,
        uint256 claimPeriod,
        uint256 numberOfClaims_,
        uint256 initialCooldown
    ) external onlyAdmin returns (bool) {
        require(!vestingTypes[type_].created, "Type already exists");
        vestingTypes[type_] = VestingType(
            true,
            initialRelease,
            releaseRate,
            claimPeriod,
            numberOfClaims_,
            initialCooldown
        );
        emit VestingTypeAdded(
            type_,
            initialRelease,
            releaseRate,
            claimPeriod,
            numberOfClaims_,
            initialCooldown
        );
        return true;
    }

    ///@dev vote to remove a vesting
    ///@param vestingType - type of the vesting to remove
    ///@return bool
    function voteRemoveVesting(string calldata vestingType)
        external
        onlyAdmin
        isVestingType(vestingType)
        canVoteRemoveVesting(vestingType)
        returns (bool)
    {
        acceptRemoveVesting[vestingType].push(msg.sender);
        emit VotedVestingType(vestingType, true);
        return true;
    }

    ///@dev remove a vesting type
    ///@param type_ - type of the vesting to remove
    ///@return bool
    function removeVestingType(string calldata type_)
        external
        onlyAdmin
        isVestingType(type_)
        canVestingBeRemoved(type_)
        returns (bool)
    {
        delete vestingTypes[type_];
        emit VestingTypeRemoved(type_);
        return true;
    }

    ///@dev vote to remove a vester
    ///@param vester - address of the vester to remove
    ///@return bool
    function voteRemoveVester(address vester)
        external
        onlyAdmin
        canVoteRemove(vester)
        returns (bool)
    {
        acceptRemoveVester[vester].push(msg.sender);
        emit Voted(vester, true);
        return true;
    }

    ///@dev remove a vester if enough admins have voted
    ///@param vester - address of the vester to remove
    ///@return bool
    function removeVester(address vester)
        external
        onlyAdmin
        canBeRemoved(vester)
        returns (bool)
    {
        delete acceptRemoveVester[vester];
        delete vestings[vester];
        emit Removed(vester);
        return true;
    }

    ///@dev claim the regular vesting
    ///@return bool
    function claim() external nonReentrant canClaim returns (bool) {
        Vesting storage vesting = vestings[msg.sender];
        uint256 alreadyClaimed = vesting.totalClaims.sub(
            vesting.numberOfClaims
        );
        uint256 lastClaim = vesting.creationTime.add(
            vesting.claimPeriod.mul(alreadyClaimed)
        );
        uint256 claimNumber = (uint256(block.timestamp).sub(lastClaim))
            .div(vesting.claimPeriod)
            .min(vesting.numberOfClaims);
        uint256 claimAmount = (
            (claimNumber.mul(vesting.rate)).mul(vesting.amount)
        ).div(uint256(10000));
        vestings[msg.sender].numberOfClaims = vesting.numberOfClaims.sub(
            claimNumber
        );
        if (vestings[msg.sender].numberOfClaims == 0) {
            claimAmount = claimAmount.add(
                vesting.amount.sub(
                    vesting
                        .totalClaims
                        .mul(vesting.rate)
                        .mul(vesting.amount)
                        .div(uint256(10000))
                )
            );
        }
        trkBEP20.transferFrom(reserve, msg.sender, claimAmount);
        emit Claimed(claimAmount);
        return true;
    }

    ///@dev Claim the initial release
    ///@return bool
    function claimFirst() external nonReentrant canClaimFirst returns (bool) {
        uint256 temp = vestings[msg.sender].firstClaim;
        vestings[msg.sender].firstClaim = uint256(0);
        trkBEP20.transferFrom(reserve, msg.sender, temp);
        emit Claimed(temp);
        return true;
    }
}
